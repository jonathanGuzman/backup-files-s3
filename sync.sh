#! /bin/sh

set -e
if [ "${S3_ACCESS_KEY_ID}" == "**None**" ]; then
  echo "You did not set the S3_ACCESS_KEY_ID environment variable."
fi

if [ "${S3_SECRET_ACCESS_KEY}" == "**None**" ]; then
  echo "You did not set the S3_SECRET_ACCESS_KEY environment variable."
fi

if [ "${S3_BUCKET}" == "**None**" ]; then
  echo "Not Valid S3_BUCKET ."
  exit 1
fi

  export AWS_ACCESS_KEY_ID=$S3_ACCESS_KEY_ID
  export AWS_SECRET_ACCESS_KEY=$S3_SECRET_ACCESS_KEY
  export AWS_DEFAULT_REGION=$S3_REGION


sync_s3 () {

  echo "Uploading files to S3... folder: $1 aws s3 sync $1  s3://$S3_BUCKET/$S3_PREFIX/"

   aws s3 sync $1  s3://$S3_BUCKET/$S3_PREFIX/

  if [ $? != 0 ]; then
    >&2 echo "Error on upload"
  fi

}

   sync_s3 /content $S3_PREFIX
