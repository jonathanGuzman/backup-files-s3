#! /bin/sh

set -e


if [ "${SCHEDULE}" = "**None**" ]; then
  sh sync.sh
else
  exec go-cron "$SCHEDULE" /bin/sh sync.sh
fi
