# Backup files to S3
This is an image to create a backup of your files in a s3 bucket.

## Install
First you have to install docker.

You can check how to install docker [here](https://gitlab.com/jonathanGuzman/getting-start-docker)

you only need to configure this variables

* AWS accces key: S3_ACCESS_KEY_ID 

* AWS secret key: S3_SECRET_ACCESS_KEY 

* Bucket Name: S3_BUCKET (is required)

* Region of S3 bucket: S3_REGION (default us-west-1)

* Folder inside the bucket: S3_PREFIX (default 'content') 

* cronjob schedule: SCHEDULE (default: run once time)

*The purpose of the docker-compose file is only if you want to customize the code through Visual Studio Code*

## How to use?

```
version: '2'

services:
  backup-files:
    image: jonathanfaz/backup-files-s3
    container_name: aws-backup-files
    volumes:
     - volumen-to-share:/content
    environment:
    S3_PREFIX: S3_PREFIX 
    #every hour
    SCHEDULE: 0 0 * * * * 
    S3_REGION: region-s3
    S3_BUCKET: bucket
    S3_ACCESS_KEY_ID: ACCESS_KEY
    S3_SECRET_ACCESS_KEY: SECRET_KEY
```
