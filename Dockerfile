FROM alpine:latest

ADD install.sh install.sh
RUN sh install.sh && rm install.sh

ENV S3_ACCESS_KEY_ID **None**
ENV S3_SECRET_ACCESS_KEY **None**
ENV S3_BUCKET **None**
ENV S3_REGION us-west-1
ENV S3_PREFIX 'content'
ENV SCHEDULE **None**

ADD run.sh run.sh
ADD sync.sh sync.sh

CMD ["sh", "run.sh"]
